# GitLab Audit Tools

A collection of useful Ruby scripts that can help automate auditing processes.

See each individual script for usage guidelines.

## Run Multiple

`run_multiple` is a script that... runs any other script multiple times.

Create a file with a single argument on each line. The script will traverse the
file line by line, running the named script with the line as input.

This is useful for copy/pasting from spreadsheet columns and running a process
on every row.

Usage:

```sh
./run_multiple <name_of_script> <source_file>
```

## Authentication

Optionally set or pass the envvar `$GITLAB_API_TOKEN` to run the script against
the api as yourself. This gives it read access to everything you have read
access to, which can be useful for ensuring all data is properly reported.

```sh
GITLAB_API_TOKEN=gl-pat_xxx ./commit_link_from_mr_link <link>
```
