require 'uri'
require 'net/http'
require 'json'

class MRUrl
  attr_reader :uri

  def initialize(url)
    @uri = URI.parse(url)
  end

  def api_url
    encoded_path = URI.encode_www_form_component(project_path)
    "https://gitlab.com/api/v4/projects/#{encoded_path}/merge_requests/#{merge_request_id}?access_token=#{ENV['GITLAB_API_TOKEN']}"
  end

  def merge_commit_sha
    @merge_commit_sha ||=
      begin
        uri = URI.parse(api_url)
        params = { access_token: $GITLAB_API_TOKEN }
        uri.query = URI.encode_www_form(params)
        response = Net::HTTP.get_response(uri)
        if response.code == "200"
          json_response = JSON.parse(response.body)
          json_response["merge_commit_sha"]
        else
          puts "Failed to fetch merge commit SHA. Response code: #{response.code}, URL: #{uri}"
          exit 1
        end
      end
  end

  def merge_commit_url
    "https://gitlab.com/#{project_path}/-/commit/#{merge_commit_sha}"
  end

  private

  def project_path
    path_segments[1..-3].join('/')
  end

  def merge_request_id
    path_segments.last
  end

  def path_segments
    uri.path.split('/').reject { |segment| segment == '-' }
  end
end

