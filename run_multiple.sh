#!/bin/bash

# Ensure user has provided a script and a file to run
if [ "$#" -ne 2 ]; then
  echo "Usage: $0 <ruby_script> <target_file>"
  exit 1
fi

ruby_script="$1"
target_file="$2"

# Check ruby script exists
if [ ! -f "$ruby_script" ]; then
  echo "Ruby script '$ruby_script' not found."
  exit 1
fi

# Check the file exists
if [ ! -f "$target_file" ]; then
  echo "File '$target_file' not found."
  exit 1
fi

# Loop through every line in the file as an argument to the script
while IFS= read -r line; do
  output=$("$ruby_script" "$line")
  echo "$output"
done < "$target_file"
