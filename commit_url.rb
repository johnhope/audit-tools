require 'uri'

class CommitUrl
  attr_reader :uri

  def initialize(url)
    @uri = URI.parse(url)
  end

  def api_url
    encoded_path = URI.encode_www_form_component(project_path)
    "https://gitlab.com/api/v4/projects/#{encoded_path}/repository/commits/#{commit_sha}"
  end

  def commit_sha
    @commit_sha ||= path_segments.last
  end

  def diff_url
    api_url + '/diff'
  end

  private

  def project_path
    path_segments[1..-3].join('/')
  end

  def merge_request_id
    path_segments.last
  end

  def path_segments
    uri.path.split('/').reject { |segment| segment == '-' }
  end
end

