require 'uri'
require 'net/http'
require 'json'

class Diff
  def initialize(url)
    @uri = URI.parse(url)
  end

  def response
    @response ||=
      begin
        uri = @uri.dup
        params = { access_token: $GITLAB_API_TOKEN }
        uri.query = URI.encode_www_form(params)
        response = Net::HTTP.get_response(uri)
        if response.code == "200"
          json_response = JSON.parse(response.body)
        else
          puts "Failed to fetch commit diff. Response code: #{response.code}, URL: #{uri}"
          exit 1
        end
      end
  end

  def log
    response.map do |file|
      filename = if file['old_path'] == file['new_path']
          file['old_path']
        elsif file['new_file']
          file['new_path']
        elsif file['deleted_file']
          file['old_path']
        else
          file['old_path'] + ' => ' + file['new_path']
        end

      # Lines that start with a + are added, - are removed, and no sign (space)
      # are unchanged.
      lines_added = file['diff'].lines.select { |l| l[0] == '+' }.size
      lines_removed = file['diff'].lines.select { |l| l[0] == '-' }.size

      "#{filename} +#{lines_added} / -#{lines_removed}"
    end
  end
end

